package cn.gayaguoguo.guohealthindicator;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.java.JavaPlugin;

import cn.gayaguoguo.guohealthindicator.listener.AttackListener;
import cn.gayaguoguo.guohealthindicator.manager.ConfigManager;
import cn.gayaguoguo.guohealthindicator.manager.HealthBarManager;
import cn.gayaguoguo.guohealthindicator.task.CheckHealthTask;
import cn.gayaguoguo.guohealthindicator.task.HealthBarRefreshTask;
import cn.gayaguoguo.guohealthindicator.util.Metrics;

public class GuoHealthIndicator extends JavaPlugin{

	private static GuoHealthIndicator instance;
	
	private ConfigManager configManager; 
	
	private HealthBarManager healthBarManager;
	
	@Override
	public void onEnable() {
		instance = this;
		healthBarManager = new HealthBarManager(instance);
		load();
		getServer().getPluginManager().registerEvents(new AttackListener(instance), instance);
		new Metrics(instance);
	}
	@Override
	public void onDisable() {
		healthBarManager.clearAllBar();
	}
	public void load(){
		configManager = new ConfigManager(instance);
		HealthBarRefreshTask.init(instance);
		CheckHealthTask.init(instance);
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (!sender.isOp()){
			return false;
		}
		if (args.length == 1){
			if (args[0].equalsIgnoreCase("reload")){
				load();
				sender.sendMessage("§e§l[GuoHealthIndicator] §a插件重载完成");
				return false;
			}
		}
		sender.sendMessage("§e§l[GuoHealthIndicator] §7->");
		sender.sendMessage("§7/ghi reload §6重载插件");
		return false;
	}
	
	@Override
	public void reloadConfig() {

	}
	
	public static GuoHealthIndicator getInstance() {
		return instance;
	}
	public ConfigManager getConfigManager() {
		return configManager;
	}
	public HealthBarManager getHealthBarManager(){
		return healthBarManager;
	}
	
}
