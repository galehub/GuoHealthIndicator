package cn.gayaguoguo.guohealthindicator.task;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.LivingEntity;
import org.bukkit.scheduler.BukkitRunnable;

import com.gmail.filoghost.holographicdisplays.api.Hologram;
import com.gmail.filoghost.holographicdisplays.api.HologramsAPI;

import cn.gayaguoguo.guohealthindicator.GuoHealthIndicator;
import cn.gayaguoguo.guohealthindicator.util.RoundingUtil;

public class CheckHealthTask {
	
	private static Integer taskID = -1;
	
	private static ConcurrentHashMap<UUID, Double> healthMap;
	
	public static void init(GuoHealthIndicator plugin){
		Long checkInterval = plugin.getConfigManager().checkInterval;
		if (checkInterval < 0){
			return;
		}
		healthMap = new ConcurrentHashMap<>();
		if (taskID != -1){
			Bukkit.getScheduler().cancelTask(taskID);
		}
		BukkitRunnable bukkitRunnable = new BukkitRunnable() {
			
			@Override
			public void run() {

				for (World world : Bukkit.getWorlds()){
					List<LivingEntity> livingEntities = new ArrayList<>(world.getLivingEntities());
					new BukkitRunnable() {
						
						@Override
						public void run() {

							for (LivingEntity livingEntity : livingEntities){
								UUID uuid = livingEntity.getUniqueId();
								Double health = livingEntity.getHealth();
								if (!healthMap.containsKey(uuid)){
									healthMap.put(uuid, health);
									continue;
								}
								if (healthMap.get(uuid) - health != 0){
									createHealthChangeTip(livingEntity.getLocation(), healthMap.get(uuid), health, plugin);
									healthMap.put(uuid, health);
								}
							}
							
						}
					}.runTaskAsynchronously(plugin);
				}
				
			}
		};
		taskID = bukkitRunnable.runTaskTimer(plugin, checkInterval, checkInterval).getTaskId();
	}
	
	public static void createHealthChangeTip(Location location, Double before, Double now, GuoHealthIndicator plugin){
		new BukkitRunnable() {
			
			@Override
			public void run() {
				Double value = now - before;
				String info;
				if (value > 0){
					info = plugin.getConfigManager().format_add.replace("%amount%", RoundingUtil.format(value));
				}else {
					value = -value;
					info = plugin.getConfigManager().format_def.replace("%amount%", RoundingUtil.format(value));
				}
				Hologram hologram = HologramsAPI.createHologram(plugin, nextLocation(location));
				hologram.appendTextLine(info);
				new BukkitRunnable() {
					
					@Override
					public void run() {

						hologram.delete();
						
					}
				}.runTaskLater(plugin, plugin.getConfigManager().HDduration);
				
			}
		}.runTask(plugin);
	}
	public static Location nextLocation(Location location){
		return location.add(getRandom(0.5, 0.6), getRandom(0D, 0.4) + 1, getRandom(0.5, 0.6));
	}
	public static Double getRandom(Double min, Double max){
		double result = min + Math.random() * (max - min);
		if (Math.random() < 0.5){
			return -result;
		}
		return result;
	}

}
