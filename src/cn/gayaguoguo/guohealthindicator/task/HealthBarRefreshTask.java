package cn.gayaguoguo.guohealthindicator.task;

import org.bukkit.Bukkit;
import org.bukkit.scheduler.BukkitRunnable;

import cn.gayaguoguo.guohealthindicator.GuoHealthIndicator;
import cn.gayaguoguo.guohealthindicator.manager.HealthBarManager;

public class HealthBarRefreshTask {
	
	public static int taskID = -1;
	
	public static void init(GuoHealthIndicator plugin){
		if (taskID != -1){
			Bukkit.getScheduler().cancelTask(taskID);
		}
		
		Long relashRate = plugin.getConfigManager().reflashRate;
		
		if (relashRate < 0){
			return;
		}
		
		BukkitRunnable bukkitRunnable = new BukkitRunnable() {
			
			@Override
			public void run() {

				HealthBarManager healthBarManager = plugin.getHealthBarManager();
				
				healthBarManager.deleteAllBar();
				
				healthBarManager.clearAllBar();
				
				healthBarManager.showAllBar();
				
			}
		};
		taskID = bukkitRunnable.runTaskTimerAsynchronously(plugin, relashRate, relashRate).getTaskId();
	}

}
