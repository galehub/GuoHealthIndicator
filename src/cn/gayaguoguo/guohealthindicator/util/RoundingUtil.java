package cn.gayaguoguo.guohealthindicator.util;

import java.text.DecimalFormat;

public class RoundingUtil {
	
	
	private static DecimalFormat decimalFormat = new DecimalFormat("######0.0");
	
	public static String format(double value) {
		String str = decimalFormat.format(value);
		if (str.endsWith(".0")) {
			str = str.replace(".0", "");
		}
		return str;
	}

}
