package cn.gayaguoguo.guohealthindicator.manager;

import org.bukkit.configuration.file.FileConfiguration;

import cn.gayaguoguo.guohealthindicator.GuoHealthIndicator;
import cn.gayaguoguo.guohealthindicator.util.FileConfig;

public class ConfigManager {
	
	public String title;
	
	public Long duration;
	
	public Long reflashRate;
	
	public Long checkInterval;
	
	public Long HDduration;
	
	public String format_add;
	public String format_def;
	
	FileConfiguration config;
	
	public ConfigManager(GuoHealthIndicator plugin) {
		config = new FileConfig("config.yml", plugin).get();
		title = config.getString("setting.title").replace("&", "§");
		duration = config.getLong("setting.duration");
		reflashRate = config.getLong("setting.reflashRate");
		checkInterval = config.getLong("setting.checkInterval");
		HDduration = config.getLong("setting.HDduration");
		format_add = config.getString("setting.format.add").replace("&", "§");
		format_def = config.getString("setting.format.def").replace("&", "§");
	}

}
